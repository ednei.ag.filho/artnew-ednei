package br.com.artnew.autenticacao;

public class AutentInvalidException extends RuntimeException {

    public AutentInvalidException(String message) {
        super("Usuário e/ou senha inválido." + message);
    }
}
