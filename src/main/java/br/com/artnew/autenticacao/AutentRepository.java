package br.com.artnew.autenticacao;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class AutentRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Boolean autenticar(AutentParam autentParam) {
        /**entityManager.createNativeQuery("SELECT 1" +
                " FROM cad_usuario" +
                " WHERE usuario '" + autentParam.getUsuario() . "'" +
                " AND senha = '" + autentParam.getSenha() . "'");**/

        return autentParam.getUsuario().equals("artnew") && autentParam.getSenha().equals("artnew123");
    }

}
