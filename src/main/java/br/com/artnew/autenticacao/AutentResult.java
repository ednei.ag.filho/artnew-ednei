package br.com.artnew.autenticacao;

import br.com.artnew.usuario.UsuarioVO;

public class AutentResult {

    UsuarioVO usuarioVO;

    public UsuarioVO getUsuarioVO() {
        return usuarioVO;
    }

    public void setUsuarioVO(UsuarioVO usuarioVO) {
        this.usuarioVO = usuarioVO;
    }
}
