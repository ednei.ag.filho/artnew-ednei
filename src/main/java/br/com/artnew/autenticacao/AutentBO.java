package br.com.artnew.autenticacao;

import br.com.artnew.usuario.UsuarioBO;
import br.com.artnew.usuario.UsuarioVO;
import org.springframework.stereotype.Component;

@Component
public class AutentBO {

    private final AutentRepository autentRepository;
    private final UsuarioBO usuarioBO;

    public AutentBO(AutentRepository autentRepository,
                    UsuarioBO usuarioBO) {
        this.autentRepository = autentRepository;
        this.usuarioBO = usuarioBO;
    }

    public AutentResult autenticar(AutentParam autentParam) {
        final boolean autenticado = autentRepository.autenticar(autentParam);

        if (!autenticado) {
            throw new AutentInvalidException("");
        }

        UsuarioVO usuarioVO = usuarioBO.consultarPorUsuario(autentParam.getUsuario());
        AutentResult autentResult = new AutentResult();
        autentResult.setUsuarioVO(usuarioVO);

        return autentResult;
    }

}
