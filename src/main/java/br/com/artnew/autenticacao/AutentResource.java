package br.com.artnew.autenticacao;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/login")
public class AutentResource {

    private final AutentBO autentBO;

    public AutentResource(AutentBO autentBO) {
        this.autentBO = autentBO;
    }

    @GetMapping("/autenticar")
    public ResponseEntity<?> autenticar(@RequestBody AutentParam autentParam) {
        AutentResult autentResult = new AutentResult();

        try {
            autentResult = autentBO.autenticar(autentParam);
        } catch (AutentInvalidException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(autentResult);
    }

}
