package br.com.artnew.autenticacao;

public class AutentParam {

    private String usuario;
    private String senha;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AutentParam that = (AutentParam) o;

        if (!usuario.equals(that.usuario)) return false;
        return senha.equals(that.senha);
    }

    @Override
    public int hashCode() {
        int result = usuario.hashCode();
        result = 31 * result + senha.hashCode();
        return result;
    }
}
