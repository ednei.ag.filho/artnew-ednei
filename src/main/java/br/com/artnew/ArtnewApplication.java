package br.com.artnew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtnewApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtnewApplication.class, args);
	}

}
