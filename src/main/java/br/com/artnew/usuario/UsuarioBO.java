package br.com.artnew.usuario;

import org.springframework.stereotype.Component;

@Component
public class UsuarioBO {

    public UsuarioVO consultar(Long id) {
        UsuarioVO usuarioVO = new UsuarioVO();
        usuarioVO.setId(id);
        usuarioVO.setUsuario("artnew");
        usuarioVO.setPermissao("master");

        return usuarioVO;
    }

    public UsuarioVO consultarPorUsuario(String usuario) {
        /** entityManager.createNativeQuery("SELECT id" +
         " FROM cad_usuario" +
         " WHERE usuario '" + autentParam.getUsuario() . "'");**/

        return consultar(1L);
    }

}
