package br.com.artnew.usuario;

import br.com.artnew.autenticacao.AutentResult;

public class UsuarioVO {

    private Long id;
    private String usuario;
    private String permissao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPermissao() {
        return permissao;
    }

    public void setPermissao(String permissao) {
        this.permissao = permissao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsuarioVO that = (UsuarioVO) o;

        if (!id.equals(that.id)) return false;
        if (!usuario.equals(that.usuario)) return false;
        return permissao.equals(that.permissao);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + usuario.hashCode();
        result = 31 * result + permissao.hashCode();
        return result;
    }
}
